/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
/* Copyright 2008 Red Hat, Inc. */

/* This file should be considered to be under the same terms as the
 * main upstream source code.
 */

#include <config.h>

#include <libsoup/soup.h>

/* FIXME: soup_auth_domain_basic_set_auth_callback's first parameter
 * should be a SoupAuthDomainBasic*, not a SoupAuthDomain*, but there is
 * no way to do this currently.
 */

/* FIXME: soup_auth_domain_digest_set_auth_callback's first parameter
 * should be a SoupAuthDomainDigest*, not a SoupAuthDomain*, but there
 * is no way to do this currently.
 */

/**
 * soup_auth_get_protection_space:
 *
 * Return value: (element-type utf8):
 **/
/* FIXME: the above does not affect SoupAuthClass->get_protection_space */

/**
 * soup_client_context_get_address:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_client_context_get_auth_domain:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_client_context_get_socket:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_cookies_free:
 * @cookies: (element-type SoupCookie):
 *
 **/

/**
 * soup_cookies_from_request:
 * @msg:
 *
 * Return value: (element-type SoupCookie):
 **/

/**
 * soup_cookies_from_response:
 * @msg:
 *
 * Return value: (element-type SoupCookie):
 **/

/**
 * soup_cookies_to_cookie_header:
 * @cookies: (element-type SoupCookie):
 *
 **/

/**
 * soup_cookies_to_request:
 * @cookies: (element-type SoupCookie):
 * @msg:
 *
 **/

/**
 * soup_cookies_to_response:
 * @cookies: (element-type SoupCookie):
 * @msg:
 *
 **/

/**
 * soup_date_to_timeval:
 * @date:
 * @time: (out):
 **/

/**
 * soup_form_decode:
 * @encoded_form:
 *
 * Return value: (element-type utf8 utf8):
 **/

/**
 * soup_form_encode_hash:
 * @form_data_set: (element-type utf8 utf8):
 *
 **/

/**
 * soup_form_request_new_from_hash:
 * @method:
 * @uri:
 * @form_data_set: (element-type utf8 utf8):
 *
 **/

/**
 * soup_header_parse_quality_list:
 * @header:
 * @unacceptable: (out) (allow-none):
 *
 **/

/**
 * soup_headers_parse_request:
 * @str:
 * @len:
 * @req_headers:
 * @req_method: (out) (allow-none):
 * @req_path: (out) (allow-none):
 * @ver: (out) (allow-none):
 *
 **/
/* FIXME: above turns req_method and req_path into "out char **"
 * rather than "out char *"
 */

/**
 * soup_headers_parse_response:
 * @str:
 * @len:
 * @headers:
 * @ver: (out) (allow-none):
 * @status_code: (out) (allow-none):
 * @reason_phrase: (out) (allow-none):
 *
 **/
/* FIXME: above turns reason_phrase into an "out char **" rather than
 * an "out char *"
 */

/**
 * soup_headers_parse_status_line:
 * @status_line:
 * @ver: (out) (allow-none):
 * @status_code: (out) (allow-none):
 * @reason_phrase: (out) (allow-none):
 *
 **/
/* FIXME: above turns reason_phrase into an "out char **" rather than
 * an "out char *"
 */

/**
 * soup_logger_attach:
 *
 * Deprecated: Use soup_session_add_feature()
 **/

/**
 * soup_logger_detach:
 *
 * Deprecated: Use soup_session_remove_feature()
 **/

/* FIXME: SOUP_MESSAGE_OVERWRITE_CHUNKS in SoupMessageFlags is
 * deprecated, but there is no way to mark a single enum value
 * deprecated.
 */

/**
 * soup_message_body_append:
 * @data: (array length=length) (element-type guint8):
 * @length:
 *
 **/

/**
 * soup_message_headers_iter_init:
 * @iter: (out) (transfer none):
 * @hdrs:
 *
 **/

/**
 * soup_message_headers_iter_next:
 * @iter: (inout) (transfer none):
 * @name: (out) (transfer none):
 * @value: (out) (transfer none):
 *
 **/
/* FIXME: above turns name and value into "out char **" rather than
 * "out char *"
 */

/**
 * soup_message_set_response:
 * @content_type: (allow-none):
 * @resp_body: (array length=resp_length):
 */

/**
 * SoupServerCallback:
 * @server:
 * @msg:
 * @path:
 * @query: (element-type utf8 utf8) (allow-none):
 * @client:
 * @user_data:
 *
 */

/**
 * soup_server_add_handler:
 * @path: (allow-none):
 */

/**
 * soup_server_get_async_context:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_server_get_listener:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_session_get_async_context:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_session_queue_message:
 * @session:
 * @msg: (transfer full):
 * @callback: (allow-none):
 * @user_data: (allow-none):
 *
 **/

/**
 * soup_socket_get_local_address:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_socket_get_remote_address:
 *
 * Return value: (transfer none):
 **/

/**
 * soup_xmlrpc_extract_method_call:
 * @method_call:
 * @length:
 * @method_name: (out):
 *
 **/
/* FIXME: above turns method_name into an "out char **" rather than
 * an "out char *"
 */

/**
 * soup_xmlrpc_parse_method_call:
 * @method_call:
 * @length:
 * @method_name: (out):
 * @params: (out):
 *
 **/
/* FIXME: above turns method_name into an "out char **" rather than
 * an "out char *"
 */

/**
 * soup_xmlrpc_parse_method_response:
 * @method_response:
 * @length:
 * @value: (out):
 *
 **/
