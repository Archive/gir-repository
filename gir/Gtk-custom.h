/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
/* Copyright 2008 litl, LLC. All Rights Reserved. */

#ifndef __GI_REPO_GTK_CUSTOM_H__
#define __GI_REPO_GTK_CUSTOM_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#ifdef GDK_WINDOWING_X11
guint32         gtk_window_get_xid                    (GtkWindow            *window);
#endif
GtkWidgetFlags  gtk_widget_get_flags                  (GtkWidget            *widget);
GtkRequisition* gtk_requisition_new                   (void);
int             gtk_requisition_get_width             (const GtkRequisition *req);
int             gtk_requisition_get_height            (const GtkRequisition *req);
GtkAllocation*  gtk_allocation_new                    (void);
int             gtk_allocation_get_x                  (const GtkAllocation *all);
int             gtk_allocation_get_y                  (const GtkAllocation *all);
int             gtk_allocation_get_width              (const GtkAllocation *all);
int             gtk_allocation_get_height             (const GtkAllocation *all);
GtkTextIter*    gtk_text_iter_new                     (void);
GtkTreeIter*    gtk_tree_iter_new                     (void);
GtkWidget*      gtk_dialog_get_vbox                   (GtkDialog            *dialog);

G_END_DECLS

#endif  /* __GI_REPO_GTK_CUSTOM_H__ */
