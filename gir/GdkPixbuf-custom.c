/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */


#define GDK_PIXBUF_ENABLE_BACKEND /* just to get the GdkPixbufFormat struct */
#include <gdk-pixbuf/gdk-pixbuf.h>
#undef GDK_PIXBUF_ENABLE_BACKEND

GdkPixbufFormat*
gdk_pixbuf_format_copy (const GdkPixbufFormat *format)
{
  GdkPixbufFormat *new_format;

  g_return_val_if_fail (format != NULL, NULL);

  new_format = g_slice_new (GdkPixbufFormat);
  *new_format = *format;
  return new_format;
}

void
gdk_pixbuf_format_free (GdkPixbufFormat *format)
{
  g_return_if_fail (format != NULL);

  g_slice_free (GdkPixbufFormat, format);
}

GType
gdk_pixbuf_format_get_type (void)
{
  static GType our_type = 0;

  if (our_type == 0)
    our_type = g_boxed_type_register_static (g_intern_static_string ("GdkPixbufFormat"),
                         (GBoxedCopyFunc)gdk_pixbuf_format_copy,
                         (GBoxedFreeFunc)gdk_pixbuf_format_free);
  return our_type;
}

/**
 * gdk_pixbuf_get_formats:
 * Returns: (element-type PixbufFormat) (transfer container)
 */

/**
 * gdk_pixbuf_format_get_extensions:
 * Returns: (array zero-terminated=1) (transfer full):
 */

/**
 * gdk_pixbuf_format_get_mime_types:
 * Returns: (array zero-terminated=1) (transfer full):
 */

/**
 * gdk_pixbuf_animation_get_static_image:
 * Returns: (transfer none):
 */
