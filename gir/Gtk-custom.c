/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
/* Copyright 2008 litl, LLC. All Rights Reserved. */

/* This file should be considered to be under the same terms as the
 * main upstream source code.
 */

#include <config.h>

#include "Gtk-custom.h"
#include <string.h>

/**************************************************
 * This section contains convenience API for Gtk+
 * which makes it easier to access parts of the API
 * which are not easily wrappable nor completely supported
 * by gobject-introspection
 */

#ifdef GDK_WINDOWING_X11
#include <gdk/gdkx.h>

guint32
gtk_window_get_xid(GtkWindow *window)
{
    g_return_val_if_fail(GTK_WIDGET_REALIZED(window), 0);

    return GDK_WINDOW_XID(GTK_WIDGET(window)->window);
}
#endif

GtkWidgetFlags
gtk_widget_get_flags(GtkWidget *widget)
{
    g_return_val_if_fail(GTK_IS_WIDGET(widget), 0);
    return GTK_WIDGET_FLAGS(widget);
}

GtkRequisition*
gtk_requisition_new(void)
{
    GtkRequisition req = { 0, 0 };
    /* using copy ensures we use the right allocator */
    return gtk_requisition_copy(&req);
}

int
gtk_requisition_get_width(const GtkRequisition *req)
{
    return req->width;
}

int
gtk_requisition_get_height(const GtkRequisition *req)
{
    return req->height;
}

GtkAllocation*
gtk_allocation_new(void)
{
    return g_new0(GtkAllocation, 1);
}

int
gtk_allocation_get_x(const GtkAllocation *all)
{
    return all->x;
}

int
gtk_allocation_get_y(const GtkAllocation *all)
{
    return all->y;
}

int
gtk_allocation_get_width(const GtkAllocation *all)
{
    return all->width;
}

int
gtk_allocation_get_height(const GtkAllocation *all)
{
    return all->height;
}

GtkTextIter*
gtk_text_iter_new(void)
{
    GtkTextIter iter;

    return gtk_text_iter_copy(&iter);
}

GtkTreeIter*
gtk_tree_iter_new(void)
{
    GtkTreeIter iter;

    return gtk_tree_iter_copy(&iter);
}

/**
 * gtk_dialog_get_vbox:
 * @dialog: A #GtkDialog
 *
 * Return value: (transfer none): the internal #GtkVBox
 */
GtkWidget *
gtk_dialog_get_vbox(GtkDialog *dialog)
{
    return dialog->vbox;
}



/****************************************************************
 * This section contains annotations which should go upstream as
 * soon as Gtk+ depends on gobject-introspection
 *
 * Annotation/API review status:
 *
 * Return value transfer
 * gtk_container_*: 2.12: Johan Dahlin
 * gtk_entry_*: 2.12: Johan Dahlin
 * gtk_widget_get_*: 2.12: Johan Dahlin
 * gtk_window_get_*: 2.12: Johan Dahlin
 * GSList/GList*: 2.14: Johan Dahlin
 *
 */

/**
 * gtk_about_dialog_get_name:
 * @about: a #GtkAboutDialog
 * Deprecated: 2.12: Use gtk_about_dialog_get_program_name() instead.
 **/

/**
 * gtk_about_dialog_set_comments: 
 * @comments: (allow-none): 
 */

/**
 * gtk_about_dialog_set_copyright: 
 * @copyright: (allow-none): 
 */

/**
 * gtk_about_dialog_set_license: 
 * @license: (allow-none): 
 */

/**
 * gtk_about_dialog_set_logo: 
 * @logo: (allow-none): 
 */

/**
 * gtk_about_dialog_set_logo_icon_name: 
 * @icon_name: (allow-none): 
 */

/**
 * gtk_about_dialog_set_name: 
 * @name: (allow-none): 
 */

/**
 * gtk_about_dialog_set_translator_credits: 
 * @translator_credits: (allow-none): 
 */

/**
 * gtk_about_dialog_set_version: 
 * @version: (allow-none): 
 */

/**
 * gtk_about_dialog_set_website: 
 * @website: (allow-none): 
 */

/**
 * gtk_about_dialog_set_website_label: 
 * @website_label: (allow-none): 
 */

/**
 * gtk_accel_group_from_object: 
 *
 * Return value: (element-type GtkAccelGroup) (transfer none):
 */

/**
 * gtk_action_create_icon:
 *
 * @icon_size: (type int):
 */

/**
 * gtk_action_group_add_action_with_accel:
 *
 * @accelerator: (allow-none): 
 */

/**
 * gtk_action_group_get_action:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_action_group_get_proxies:
 *
 * Return value: (element-type GtkWidget) (transfer none):
 */

/**
 * gtk_action_group_list_actions:
 *
 * Return value: (element-type GtkAction) (transfer container):
 */

/**
 * gtk_action_set_accel_group: 
 * @accel_group: (allow-none): 
 */

/**
 * gtk_alternative_dialog_button_order: 
 * @screen: (allow-none): 
 */

/**
 * gtk_assistant_set_page_header_image: 
 * @pixbuf: (allow-none): 
 */

/**
 * gtk_assistant_set_page_side_image: 
 * @pixbuf: (allow-none): 
 */

/**
 * gtk_bin_get_child:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_box_pack_start_defaults:
 * @box: a #GtkBox
 * @widget: the #GtkWidget to be added to @box
 *
 * Deprecated: 2.14: Use gtk_box_pack_start()
 */

/**
 * gtk_box_pack_start: 
 * @child: (transfer full): 
 */

/**
 * gtk_builder_get_object:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_builder_get_objects:
 *
 * Return value: (element-type GObject) (transfer container):
 */

/**
 * gtk_cell_layout_get_cell_renderers:
 *
 * Return value: (element-type GtkCellRenderer) (transfer container):
 */

/**
 * gtk_cell_view_set_displayed_row: 
 * @path: (allow-none): 
 */

/**
 * gtk_cell_view_set_model: 
 * @model: (allow-none): 
 */

/**
 * gtk_clipboard_wait_for_uris:
 *
 * Return value: (array zero-terminated=1) (element-type utf8) (transfer full):
 */

/**
 * gtk_clist_set_pixmap: 
 * @mask: (allow-none): 
 */

/* FIXME: This should be done in gobject-introspection itself,
          instead of here. It probably applies to all alternative
          constructors.
          Verify that GJS works before removing this.
          Johan 2008-12-09
 */
/**
 * gtk_combo_box_new_text:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_combo_box_get_model:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_combo_box_set_model: 
 * @model: (allow-none): 
 */

/**
 * gtk_container_get_children: 
 * @container: 
 *
 * Return value: (element-type GtkWidget) (transfer container): List of #GtkWidget
 */

/**
 * gtk_container_get_focus_chain:
 * @focusable_widgets: (element-type Gtk.Widget) (out) (transfer container):
 *
 * Return value: boolean:
 */

/**
 * gtk_container_get_focus_hadjustment: 
 *
 * Return value: (transfer none): 
 */

/**
 * gtk_container_get_focus_vadjustment: 
 *
 * Return value: (transfer none): 
 */

/**
 * gtk_container_set_focus_child: 
 * @child: (allow-none): 
 */

/**
 * gtk_container_add: 
 * @widget: (transfer full): 
 */

/**
 * gtk_ctree_insert_node: 
 * @pixmap_closed: (allow-none): 
 * @mask_closed: (allow-none): 
 * @pixmap_opened: (allow-none): 
 * @mask_opened: (allow-none): 
 */

/**
 * gtk_ctree_move: 
 * @new_parent: (allow-none): 
 * @new_sibling: (allow-none): 
 */

/**
 * gtk_ctree_node_set_pixmap: 
 * @mask: (allow-none): 
 */

/**
 * gtk_ctree_node_set_pixtext: 
 * @mask: (allow-none): 
 */

/**
 * gtk_ctree_set_node_info: 
 * @pixmap_closed: (allow-none): 
 * @mask_closed: (allow-none): 
 * @pixmap_opened: (allow-none): 
 * @mask_opened: (allow-none): 
 */

/**
 * gtk_dialog_new_with_buttons: 
 * @title: (allow-none): 
 * @parent: (allow-none): 
 * @first_button_text: (allow-none): 
 */

/**
 * gtk_drag_source_set_icon: 
 * @mask: (allow-none): 
 */

/**
 * gtk_draw_insertion_cursor: 
 * @area: (allow-none): 
 */

/**
 * gtk_editable_insert_text: 
 * @position: (in-out): 
 */

/**
 * gtk_editable_get_selection_bounds: 
 * @start: (out): 
 * @end: (out): 
 */

/**
 * gtk_entry_completion_set_model: 
 * @model: (allow-none): 
 */

/**
 * gtk_entry_get_cursor_hadjustment: 
 *
 * Return value: (transfer none)
 */

/**
 * gtk_entry_get_inner_border: 
 *
 * Return value: (transfer none)
 */

/**
 * gtk_entry_get_layout: 
 *
 * Return value: (transfer none)
 */

/**
 * gtk_entry_set_completion: 
 * @completion: (allow-none): 
 */

/**
 * gtk_entry_set_inner_border: 
 * @border: (allow-none): 
 */

/**
 * gtk_expander_new_with_mnemonic: 
 * @label: (allow-none): 
 */

/**
 * gtk_expander_set_label: 
 * @label: (allow-none): 
 */

/**
 * gtk_expander_set_label_widget: 
 * @label_widget: (allow-none): 
 */

/**
 * gtk_file_chooser_get_filenames: 
 *
 * Return value: (element-type utf8) (transfer full):
 */

/**
 * gtk_file_chooser_get_uris: 
 *
 * Return value: (element-type utf8) (transfer full): 
 */

/**
 * gtk_file_chooser_get_files: 
 *
 * Return value: (element-type utf8) (transfer full): 
 */

/**
 * gtk_file_chooser_list_filters: 
 *
 * Return value: (element-type utf8) (transfer container): 
 */

/**
 * gtk_file_chooser_list_shortcut_folders: 
 *
 * Return value: (element-type utf8) (transfer full): 
 */

/**
 * gtk_file_chooser_list_shortcut_folder_uris: 
 *
 * Return value: (element-type utf8) (transfer full): 
 */

/**
 * gtk_frame_set_label: 
 * @label: (allow-none): 
 */

/**
 * gtk_icon_info_get_attach_points:
 * @points: (array length=n_points) (out):
 * @n_points:
 *
 * Return value: boolean:
 */

/**
 * gtk_icon_theme_get_search_path:
 * @path: (array length=n_elements) (out):
 * @n_elements:
 */

/**
 * gtk_icon_set_get_sizes:
 * @sizes: (array length=n_sizes) (out) (type int):
 * @n_sizes:
 *
 * FIXME. the (type int) annotation does not work
 */

/**
 * gtk_icon_set_render_icon: 
 * @size: (type int):
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_icon_size_from_name:
 *
 * Return value: (type int):
 */

/**
 * gtk_icon_size_get_name:
 * @size: (type int):
 */

/**
 * gtk_icon_size_lookup:
 * @size: (type int):
 */

/**
 * gtk_icon_size_lookup_for_settings:
 * @size: (type int):
 */

/**
 * gtk_icon_size_register:
 *
 * Return value: (type int):
 */

/**
 * gtk_icon_size_register_alias:
 * @target: (type int):
 */

/**
 * gtk_icon_source_get_size:
 * Return value: (type int):
 */

/**
 * gtk_icon_source_set_icon_name: 
 * @icon_name: (allow-none): 
 */

/**
 * gtk_icon_source_set_size:
 * @size: (type int):
 */

/**
 * gtk_icon_theme_get_default:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_icon_theme_get_for_screen:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_icon_theme_list_contexts:
 *
 * Return value: (element-type utf8) (transfer full):
 */

/**
 * gtk_icon_theme_list_icons:
 *
 * Return value: (element-type utf8) (transfer none):
 */

/**
 * gtk_icon_info_get_builtin_pixbuf:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_icon_view_get_selected_items:
 *
 * Return value: (element-type GtkTreePath) (transfer full): 
 */

/**
 * gtk_icon_view_set_cursor: 
 * @cell: (allow-none): 
 */

/**
 * gtk_icon_view_set_drag_dest_item: 
 * @path: (allow-none): 
 */

/**
 * gtk_icon_view_set_model: 
 * @model: (allow-none): 
 */

/**
 * gtk_image_menu_item_set_image: 
 * @image: (allow-none) (transfer full):
 */

/**
 * gtk_image_get_animation:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_image_get_gicon:
 * @gicon: (out) (transfer none):
 * @size: (out) (type int):
 */

/**
 * gtk_image_get_icon_name:
 * @icon_name: (out) (transfer none):
 * @size: (out) (type int):
 */

/**
 * gtk_image_get_icon_set:
 * @icon_set: (out) (transfer none):
 * @size: (out) (type int):
 */

/**
 * gtk_image_get_pixbuf:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_image_get_stock:
 * @stock_id: (out) (transfer none):
 * @size: (out) (type int):
 */

/**
 * gtk_image_new_from_gicon:
 * @size: (type int):
 */

/**
 * gtk_image_new_from_icon_name:
 * @size: (type int):
 */

/**
 * gtk_image_new_from_icon_set:
 * @size: (type int):
 */

/**
 * gtk_image_new_from_image: 
 * @image: (allow-none): 
 * @mask: (allow-none): 
 */

/**
 * gtk_image_new_from_pixbuf: 
 * @pixbuf: (allow-none): 
 */

/**
 * gtk_image_new_from_pixmap: 
 * @pixmap: (allow-none): 
 * @mask: (allow-none): 
 */

/**
 * gtk_image_new_from_stock:
 * @size: (type int):
 */

/**
 * gtk_image_set_from_file: 
 * @filename: (allow-none): 
 */

/**
 * gtk_image_set_from_gicon:
 * @size: (type int):
 */

/**
 * gtk_image_set_from_icon_name:
 * @size: (type int):
 */

/**
 * gtk_image_set_from_icon_set:
 * @size: (type int):
 */

/**
 * gtk_image_set_from_image: 
 * @gdk_image: (allow-none): 
 * @mask: (allow-none): 
 */

/**
 * gtk_image_set_from_pixbuf: 
 * @pixbuf: (allow-none): 
 */

/**
 * gtk_image_set_from_pixmap: 
 * @mask: (allow-none): 
 */

/**
 * gtk_image_set_from_stock:
 * @size: (type int):
 */

/**
 * gtk_init:
 * @argc: (inout):
 * @argv: (array length=argc) (inout) (allow-none):
 */

/**
 * gtk_init_check:
 * @argc: (inout):
 * @argv: (array length=argc) (inout) (allow-none):
 */

/**
 * gtk_init_with_args:
 * @argc: (inout):
 * @argv: (array length=argc) (inout) (allow-none):
 */

/**
 * gtk_item_factory_new: 
 * @accel_group: (allow-none): 
 */

/**
 * gtk_label_set_mnemonic_widget: 
 * @widget: (allow-none): 
 */

/**
 * gtk_layout_set_hadjustment: 
 * @adjustment: (allow-none): 
 */

/**
 * gtk_layout_set_vadjustment: 
 * @adjustment: (allow-none): 
 */

/**
 * gtk_label_get_layout:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_link_button_new_with_label: 
 * @label: (allow-none): 
 */

/**
 * gtk_list_store_newv: 
 * @n_columns: 
 * @types: (array length=n_columns): List of types
 */

/**
 * gtk_list_store_set_valuesv:
 * @iter:
 * @columns: (array length=n_values): List of columns
 * @values: (array length=n_values): List of values
 * @n_values:
 */

/**
 * gtk_list_store_set_column_types: 
 * @list_store: a #GtkListStore
 * @n_columns: 
 * @types: (array length=n_columns): List of types
 */

/**
 * gtk_list_store_move_after: 
 * @position: (allow-none): 
 */

/**
 * gtk_list_store_move_before: 
 * @position: (allow-none): 
 */

/**
 * gtk_list_store_append:
 * @position: (allow-none):
 */

/**
 * gtk_menu_get_for_attach_widget: 
 *
 * Return value: (element-type GtkWidget) (transfer none):
 */

/**
 * gtk_menu_item_set_accel_path: 
 * @accel_path: (allow-none): 
 */

/**
 * gtk_menu_set_accel_group: 
 * @accel_group: (allow-none): 
 */

/**
 * gtk_menu_set_accel_path: 
 * @accel_path: (allow-none): 
 */

/**
 * gtk_menu_set_screen: 
 * @screen: (allow-none): 
 */

/**
 * gtk_menu_tool_button_set_arrow_tooltip: 
 * @tip_text: (allow-none): 
 * @tip_private: (allow-none): 
 */

/**
 * gtk_message_dialog_new: 
 * @parent: (allow-none): 
 * @message_format: (allow-none): 
 */

/**
 * gtk_notebook_append_page: 
 * @tab_label: (allow-none): 
 */

/**
 * gtk_notebook_get_nth_page:
 * Return value: (transfer none):
 */

/**
 * gtk_notebook_get_tab_label:
 * Return value: (transfer none):
 */

/**
 * gtk_notebook_append_page_menu: 
 * @tab_label: (allow-none): 
 * @menu_label: (allow-none): 
 */

/**
 * gtk_notebook_insert_page: 
 * @tab_label: (allow-none): 
 */

/**
 * gtk_notebook_insert_page_menu: 
 * @tab_label: (allow-none): 
 * @menu_label: (allow-none): 
 */

/**
 * gtk_notebook_prepend_page: 
 * @tab_label: (allow-none): 
 */

/**
 * gtk_notebook_prepend_page_menu: 
 * @tab_label: (allow-none): 
 * @menu_label: (allow-none): 
 */

/**
 * gtk_notebook_set_menu_label: 
 * @menu_label: (allow-none): 
 */

/**
 * gtk_notebook_set_tab_label: 
 * @tab_label: (allow-none): 
 */

/**
 * gtk_paint_arrow: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_box: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_box_gap: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_check: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_diamond: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_expander: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_extension: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_flat_box: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_focus: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_handle: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_hline: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_layout: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_option: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_polygon: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_resize_grip: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_shadow: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_shadow_gap: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_slider: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_string: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_tab: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paint_vline: 
 * @area: (allow-none): 
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_paper_size_new: 
 * @name: (allow-none): 
 */

/**
 * gtk_paper_size_get_papers_sizes: 
 *
 * Return value: (element-type GtkPaperSize) (transfer full): 
 */

/**
 * gtk_parse_args:
 * @argc: (inout): 
 * @argv: (array) (inout): 
 */

/**
 * gtk_pixmap_new: 
 * @mask: (allow-none): 
 */

/**
 * gtk_pixmap_set: 
 * @mask: (allow-none): 
 */

/**
 * gtk_printer_backend_get_printer_list: 
 *
 * Return value: (element-type GtkPrinter) (transfer container): 
 */

/**
 * gtk_printer_backend_load_modules: 
 *
 * Return value: (element-type GtkPrintBackend) (transfer container): 
 */

/**
 * gtk_print_operation_run: 
 * @parent: (allow-none): 
 */

/**
 * gtk_print_operation_set_default_page_setup: 
 * @default_page_setup: (allow-none): 
 */

/**
 * gtk_print_operation_set_print_settings: 
 * @print_settings: (allow-none): 
 */

/**
 * gtk_print_run_page_setup_dialog: 
 * @parent: (allow-none): 
 * @page_setup: (allow-none): 
 */

/**
 * gtk_print_settings_foreach: 
 * @user_data: (allow-none): 
 */

/**
 * gtk_print_settings_set: 
 * @value: (allow-none): 
 */

/**
 * gtk_printer_list_papers: 
 *
 * Return value: (element-type GtkPageSetup) (transfer full): 
 */

/**
 * gtk_printer_option_set_get_groups: 
 *
 * Return value: (element-type utf8) (transfer full): 
 */

/**
 * gtk_progress_bar_new_with_adjustment: 
 * @adjustment: (allow-none): 
 */

/**
 * gtk_radio_action_get_group:
 *
 * Return value: (element-type GtkAction) (transfer none):
 */

/**
 * gtk_radio_button_get_group:
 *
 * Return value: (element-type GtkRadioButton) (transfer none):
 */

/**
 * gtk_radio_menu_item_get_group:
 *
 * Return value: (element-type GtkRadioMenuItem) (transfer none):
 */

/**
 * gtk_radio_menu_item_new_with_label:
 *
 * @group: (element-type GtkRadioMenuItem) (transfer full):
 */

/**
 * gtk_radio_menu_item_set_group:
 *
 * @group: (element-type GtkRadioMenuItem) (transfer full):
 */

/**
 * gtk_rc_get_style_by_paths: 
 * @widget_path: (allow-none): 
 * @class_path: (allow-none): 
 */

/**
 * gtk_recent_chooser_get_items:
 *
 * Return value: (element-type GtkRecentInfo) (transfer full): List of #GtkRecentInfo
 */

/**
 * gtk_recent_chooser_list_filters:
 *
 * Return value: (element-type GtkRecentFilter) (transfer container): List of #GtkRecentFilter
 */

/**
 * gtk_recent_manager_get_items:
 * @manager: 
 *
 * Return value: (element-type GtkRecentInfo) (transfer full): List of #GtkRecentInfo
 */

/**
 * gtk_recent_info_get_application_info:
 * @app_exec: (transfer none) (out):
 * @count: (out):
 * @time_: (out):
 *
 */

/**
 * gtk_recent_info_get_applications:
 * @length: (out):
 *
 * Return value: (array length=length zero-terminated=1): List of applications
 *
 */

/**
 * gtk_recent_info_get_groups:
 * @length: (out):
 *
 * Return value: (array length=length zero-terminated=1): List of groups
 *
 */

/**
 * gtk_recent_manager_get_default:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_scale_button_new:
 * @size: (type int):
 */

/**
 * gtk_scrolled_window_new: 
 * @hadjustment: (allow-none): 
 * @vadjustment: (allow-none): 
 */

/**
 * gtk_selection_data_get_uris:
 *
 * Return value: (array zero-terminated=1) (element-type utf8) (transfer full):
 */

/**
 * gtk_selection_owner_set_for_display: 
 * @widget: (allow-none): 
 */

/**
 * gtk_settings_get_default:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_size_group_get_widgets: 
 *
 * Return value: (element-type GtkWidget) (transfer none): 
 */

/**
 * gtk_spin_button_configure: 
 * @adjustment: (allow-none): 
 */

/**
 * gtk_status_icon_get_geometry:
 * @screen: (out) (transfer none):
 * @area:
 * @orientation:
 * 
 * Return value: boolean:
 */

/**
 * gtk_status_icon_set_from_pixbuf: 
 * @pixbuf: (allow-none): 
 */

/**
 * gtk_status_icon_set_tooltip: 
 * @tooltip_text: (allow-none): 
 */

/**
 * gtk_stock_list_ids: 
 *
 * Return value: (element-type utf8) (transfer full):
 */

/**
 * gtk_style_apply_default_background: 
 * @area: (allow-none): 
 */

/**
 * gtk_style_render_icon: 
 * @size: (type int):
 * @widget: (allow-none): 
 * @detail: (allow-none): 
 */

/**
 * gtk_text_buffer_create_mark: 
 * @mark_name: (allow-none): 
 */

/**
 * gtk_text_buffer_paste_clipboard: 
 * @override_location: (allow-none): 
 */

/**
 * gtk_text_buffer_register_deserialize_tagset: 
 * @tagset_name: (allow-none): 
 */

/**
 * gtk_text_buffer_register_serialize_tagset: 
 * @tagset_name: (allow-none): 
 */

/**
 * gtk_text_buffer_get_insert:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_text_buffer_get_mark:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_text_buffer_get_selection_bound:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_text_buffer_get_tag_table:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_text_mark_get_buffer:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_text_child_anchor_get_widgets: 
 *
 * Return value: (element-type GtkWidget) (transfer container): 
 */

/**
 * gtk_text_iter_backward_search: 
 * @limit: (allow-none): 
 */

/**
 * gtk_text_iter_backward_to_tag_toggle: 
 * @tag: (allow-none): 
 */

/**
 * gtk_text_iter_begins_tag: 
 * @tag: (allow-none): 
 */

/**
 * gtk_text_iter_ends_tag: 
 * @tag: (allow-none): 
 */

/**
 * gtk_text_iter_forward_search: 
 * @limit: (allow-none): 
 */

/**
 * gtk_text_iter_forward_to_tag_toggle: 
 * @tag: (allow-none): 
 */

/**
 * gtk_text_iter_get_marks: 
 *
 * Return value: (element-type GtkTextMark) (transfer container):
 */

/**
 * gtk_text_iter_get_toggled_tags:
 *
 * Return value: (element-type GtkTextTag) (transfer container):
 */

/**
 * gtk_text_iter_get_tags:
 *
 * Return value: (element-type GtkTextTag) (transfer container):
 */

/**
 * gtk_text_iter_get_buffer
 *
 * Return value: (transfer none)
 */

/**
 * gtk_text_iter_get_pixbuf
 *
 * Return value: (transfer none)
 */

/**
 * gtk_text_iter_toggles_tag: 
 * @tag: (allow-none): 
 */

/**
 * gtk_text_layout_get_lines: 
 *
 * Return value: (element-type GtkTextLine) (transfer container):
 */

/**
 * gtk_text_layout_set_buffer: 
 * @buffer: (allow-none): 
 */

/**
 * gtk_text_view_set_buffer: 
 * @buffer: (allow-none): 
 */

/**
 * gtk_text_view_get_buffer:
 * Return value: (transfer none):
 */

/**
 * gtk_text_view_get_window:
 * Return value: (transfer none):
 */


/**
 * gtk_tool_button_new: 
 * @icon_widget: (allow-none): 
 * @label: (allow-none): 
 */

/**
 * gtk_tool_button_set_icon_name: 
 * @icon_name: (allow-none): 
 */

/**
 * gtk_tool_button_set_icon_widget: 
 * @icon_widget: (allow-none) (transfer full):
 */

/**
 * gtk_tool_button_set_label: 
 * @label: (allow-none): 
 */

/**
 * gtk_tool_button_set_label_widget: 
 * @label_widget: (allow-none): 
 */

/**
 * gtk_tool_button_set_stock_id: 
 * @stock_id: (allow-none): 
 */

/**
 * gtk_tool_item_get_icon_size:
 *
 * Return value: (type int):
 */

/**
 * gtk_tool_item_set_proxy_menu_item: 
 * @menu_item: (allow-none): 
 */

/**
 * gtk_tool_item_set_tooltip: 
 * @tip_text: (allow-none): 
 * @tip_private: (allow-none): 
 */

/**
 * gtk_tool_shell_get_icon_size:
 *
 * Return value: (type int):
 */

/**
 * gtk_toolbar_append_item: 
 * @user_data: (allow-none): 
 */

/**
 * gtk_toolbar_append_widget: 
 * @tooltip_text: (allow-none): 
 * @tooltip_private_text: (allow-none): 
 */

/**
 * gtk_toolbar_get_icon_size:
 *
 * Return value: (type int):
 */

/**
 * gtk_toolbar_insert_widget: 
 * @tooltip_text: (allow-none): 
 * @tooltip_private_text: (allow-none): 
 */

/**
 * gtk_toolbar_prepend_widget: 
 * @tooltip_text: (allow-none): 
 * @tooltip_private_text: (allow-none): 
 */

/**
 * gtk_toolbar_set_drop_highlight_item: 
 * @tool_item: (allow-none): 
 */

/**
 * gtk_toolbar_insert: 
 * @item: (transfer full): 
 */

/**
 * gtk_toolbar_set_icon_size:
 * @icon_size: (type int):
 */

/**
 * gtk_tooltip_set_icon_from_icon_name:
 * @size: (type int):
 */

/**
 * gtk_tooltip_set_icon_from_stock:
 * @size: (type int):
 */

/**
 * gtk_tooltips_set_tip: 
 * @tip_text: (allow-none): 
 * @tip_private: (allow-none): 
 */

/**
 * gtk_tree_model_filter_new: 
 * @root: (allow-none): 
 */

/**
 * gtk_tree_model_iter_children: 
 * @parent: (allow-none): 
 */

/**
 * gtk_tree_model_iter_n_children: 
 * @iter: (allow-none): 
 */

/**
 * gtk_tree_model_iter_nth_child: 
 * @parent: (allow-none): 
 */

/**
 * gtk_tree_model_sort_convert_child_iter_to_iter: 
 * @sort_iter: (allow-none): 
 */

/**
 * gtk_tree_model_sort_convert_iter_to_child_iter: 
 * @child_iter: (allow-none): 
 */

/**
 * gtk_tree_selection_get_selected_rows: 
 *
 * Return value: (element-type GtkTreePath) (transfer full): 
 */

/**
 * gtk_tree_selection_get_selected: 
 * @model: (out) (allow-none): 
 */

/**
 * gtk_tree_store_move_after: 
 * @position: (allow-none): 
 */

/**
 * gtk_tree_store_move_before: 
 * @position: (allow-none): 
 */

/**
 * gtk_tree_view_append_column: 
 * @column: (transfer full): 
 */

/**
 * gtk_tree_view_column_set_widget: 
 * @widget: (allow-none): 
 */

/**
 * gtk_tree_view_column_pack_start: 
 * @cell: (transfer full): 
 */

/**
 * gtk_tree_view_get_cell_area: 
 * @column: (allow-none): 
 */

/**
 * gtk_tree_view_get_columns: 
 *
 * Return value: (element-type GtkTreeViewColumn) (transfer container): 
 */

/**
 * gtk_tree_view_move_column_after: 
 * @base_column: (allow-none): 
 */

/**
 * gtk_tree_view_scroll_to_cell: 
 * @column: (allow-none): 
 */

/**
 * gtk_tree_view_set_cursor: 
 * @focus_column: (allow-none): 
 */

/**
 * gtk_tree_view_set_cursor_on_cell: 
 * @focus_column: (allow-none): 
 * @focus_cell: (allow-none): 
 */

/**
 * gtk_tree_view_set_model: 
 * @model: (allow-none): 
 */

/**
 * gtk_tree_view_set_search_entry: 
 * @entry: (allow-none): 
 */

/**
 * gtk_ui_manager_add_ui: 
 * @action: (allow-none): 
 */

/**
 * gtk_ui_manager_get_accel_group: 
 *
 * Return value: (transfer none): 
 */

/**
 * gtk_ui_manager_get_action_groups: 
 *
 * Return value: (element-type GtkActionGroup) (transfer none): 
 */

/**
 * gtk_ui_manager_get_toplevels: 
 *
 * Return value: (element-type GtkWidget) (transfer container): 
 */

/**
 * gtk_ui_manager_get_widget: 
 *
 * Return value: (transfer none): 
 */

/**
 * gtk_viewport_set_hadjustment: 
 * @adjustment: (allow-none): 
 */

/**
 * gtk_viewport_set_vadjustment: 
 * @adjustment: (allow-none): 
 */

/**
 * gtk_widget_class_path: 
 * @path_length: (out): 
 * @path: (out): 
 * @path_reversed: (out): 
 */

/**
 * gtk_widget_destroyed:
 * @widget_pointer: (inout) (transfer none):
 */

/**
 * gtk_widget_get_allocation:
 * @widget: the widget
 * @allocation: (out): location to store the allocation
 */

/**
 * gtk_widget_get_accessible:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_ancestor:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_clipboard:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_colormap:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_default_colormap:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_default_style:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_default_visual:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_display:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_modifier_style:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_pango_context:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_parent:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_parent_window:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_pointer: 
 * @x: (out): 
 * @y: (out): 
 */

/**
 * gtk_widget_get_root_window:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_screen: 
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_settings: 
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_size_request: 
 * @width: (out): 
 * @height: (out): 
 */

/**
 * gtk_widget_get_style: 
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_tooltip_window:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_toplevel:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_get_visual:
 *
 * Return value: (transfer none):
 */

/**
 * gtk_widget_input_shape_combine_mask: 
 * @shape_mask: (allow-none): 
 */

/**
 * gtk_widget_list_mnemonic_lables: 
 *
 * Return value: (element-type GtkWidget) (transfer container):
 */

/**
 * gtk_widget_modify_base: 
 * @color: (allow-none): 
 */

/**
 * gtk_widget_modify_bg: 
 * @color: (allow-none): 
 */

/**
 * gtk_widget_modify_fg: 
 * @color: (allow-none): 
 */

/**
 * gtk_widget_modify_font: 
 * @font_desc: (allow-none): 
 */

/**
 * gtk_widget_modify_text: 
 * @color: (allow-none): 
 */

/**
 * gtk_widget_path: 
 * @path_length: (out): 
 * @path: (out): 
 * @path_reversed: (out): 
 */

/**
 * gtk_widget_render_icon: 
 * @size: (type int):
 * @detail: (allow-none): 
 */

/**
 * gtk_widget_set_accel_path: 
 * @accel_path: (allow-none): 
 * @accel_group: (allow-none): 
 */

/**
 * gtk_widget_set_scroll_adjustments: 
 * @hadjustment: (allow-none): 
 * @vadjustment: (allow-none): 
 */

/**
 * gtk_widget_set_style: 
 * @style: (allow-none): 
 */

/**
 * gtk_widget_translate_coordinates: 
 * @dest_x: (out): 
 * @dest_y: (out): 
 */

/**
 * gtk_window_get_default_icon_list: 
 * 
 * Return value: (element-type GdkPixbuf) (transfer container):
 */

/**
 * gtk_window_get_focus: 
 * 
 * Return value: (transfer none):
 */

/**
 * gtk_window_get_gravity: 
 * 
 * Return value: (transfer none):
 */

/**
 * gtk_window_get_group: 
 * 
 * Return value: (transfer none):
 */

/**
 * gtk_window_get_icon: 
 * 
 * Return value: (transfer none):
 */

/**
 * gtk_window_get_icon_list: 
 * 
 * Return value: (element-type GdkPixbuf) (transfer container):
 */

/**
 * gtk_window_get_screen: 
 * 
 * Return value: (transfer none):
 */

/**
 * gtk_window_get_size: 
 * @width: (out):  
 * @height: (out): 
 */

/**
 * gtk_window_get_transient_for: 
 * 
 * Return value: (transfer none):
 */

/**
 * gtk_window_group_list_windows: 
 * 
 * Return value: (element-type GtkWidget) (transfer container):
 */

/**
 * gtk_window_list_toplevels: 
 * 
 * Return value: (element-type GtkWidget) (transfer container):
 */

/**
 * gtk_window_set_default: 
 * @default_widget: (allow-none): 
 */

/**
 * gtk_window_set_focus: 
 * @focus: (allow-none): 
 */

/**
 * gtk_window_set_icon: 
 * @icon: (allow-none): 
 */

/**
 * gtk_window_set_icon_name: 
 * @name: (allow-none): 
 */

/**
 * gtk_window_set_transient_for: 
 * @parent: (allow-none): 
 */

/* Do not add annotations here, please keep the order alphabetically, 
 * as it will make it easier to put these upstream at a later point
 */
