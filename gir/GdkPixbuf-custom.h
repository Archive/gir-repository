/* Copyright 2010 litl, LLC. All Rights Reserved. */

#ifndef __GIREPO_GDKPIXBUF_CUSTOM_H__
#define __GIREPO_GDKPIXBUF_CUSTOM_H__

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

G_BEGIN_DECLS

GType gdk_pixbuf_format_get_type (void);

G_END_DECLS

#endif  /* __GIREPO_GDKPIXBUF_CUSTOM_H__ */
