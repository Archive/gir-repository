/* Copyright 2008 litl, LLC. All Rights Reserved. */

/* This file should be considered to be under the same terms as the
 * main upstream source code.
 */

#include <config.h>

#include "Gdk-custom.h"

GdkRectangle*
gdk_rectangle_new (void)
{
  return g_new0 (GdkRectangle, 1);
}

guint
gdk_event_get_symbol (GdkEvent *event)
{
  return event->key.keyval;
}

/**
 * gdk_init:
 * @argc: (inout): 
 * @argv: (array length=argc) (inout): 
 */

/**
 * gdk_init_check:
 * @argc: (inout): 
 * @argv: (array length=argc) (inout): 
 */

/**
 * gdk_pixmap_create_from_xpm:
 * @mask: (out): 
 */

/**
 * gdk_pixmap_create_from_xpm_d:
 * @mask: (out): 
 */

/**
 * gdk_color_parse:
 * @color: (out): 
 */

/**
 * gdk_pixmap_colormap_create_from_xpm:
 * @mask: (out): 
 */

/**
 * gdk_pixmap_colormap_create_from_xpm_d:
 * @mask: (out): 
 */

/**
 * gdk_device_get_state:
 * @mask: (out): 
 */

/**
 * gdk_device_free_history:
 * @events: (inout) (transfer none):
 * @n_events:
 */

/**
 * gdk_device_get_history:
 * @window:
 * @start:
 * @stop:
 * @events: (array length=n_events) (out) (transfer none):
 * @n_events:
 */

/**
 * gdk_drag_find_window:
 * @dest_window: (out): 
 * @protocol: (out): 
 */

/**
 * gdk_drag_find_window_for_screen:
 * @dest_window: (out): 
 * @protocol: (out): 
 */

/**
 * gdk_drawable_get_size:
 * @width: (out): 
 * @height: (out): 
 */

/**
 * gdk_display_get_default:
 * Return value: (transfer none):
 */

/**
 * gdk_display_get_pointer:
 * @screen: (out) (transfer none):
 * @x: (out): 
 * @y: (out): 
 * @mask: (out): 
 */

/**
 * gdk_display_get_window_at_pointer:
 * @win_x: (out): 
 * @win_y: (out): 
 */

/**
 * gdk_display_get_maximal_cursor_size:
 * @width: (out): 
 * @height: (out): 
 */

/**
 * gdk_keyval_convert_case:
 * @lower: (out): 
 * @upper: (out): 
 */

/**
 * gdk_keymap_get_entries_for_keyval:
 * @keys: (out): 
 * @n_keys: (out): 
 */

/**
 * gdk_keymap_get_entries_for_keycode:
 * @keys: (out): 
 * @n_entries: (out): 
 */

/**
 * gdk_keymap_translate_keyboard_state:
 * @keyval: (out): 
 * @effective_group: (out): 
 * @level: (out): 
 * @consumed_modifiers: (out): 
 */

/**
 * gdk_query_depths:
 * @depths: (out): 
 * @count: (out): 
 */

/**
 * gdk_window_at_pointer:
 * @win_x: (out): 
 * @win_y: (out): 
 */

/**
 * gdk_window_new:
 * @parent: (allow-none): 
 */

/**
 * gdk_display_get_window_at_pointer:
 * @win_x: (out): 
 * @win_y: (out): 
 */

/**
 * gdk_region_get_rectangles:
 * @rectangles: (array length=n_rectangles) (transfer container):
 * @n_rectangles:
 **/

/**
 * gdk_screen_get_default:
 *
 * Return value: (transfer none):
 */

/**
 * gdk_screen_get_rgb_colormap:
 *
 * Return value: (transfer none):
 */

/**
 * gdk_screen_get_rgb_visual:
 *
 * Return value: (transfer none):
 */

/**
 * gdk_screen_get_rgba_colormap:
 *
 * Return value: (transfer none):
 */

/**
 * gdk_screen_get_rgba_visual:
 *
 * Return value: (transfer none):
 */

/**
 * gdk_screen_get_root_window:
 *
 * Return value: (transfer none):
 */

/**
 * gdk_window_get_internal_paint_info:
 * @real_drawable: (out):
 * @x_offset:
 * @y_offset:
 */

/**
 * gdk_window_get_pointer:
 * @x: (out):
 * @y: (out):
 * @mask: (out):
 *
 * Return value: (transfer none):
 */
