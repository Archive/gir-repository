/* Copyright 2008 litl, LLC. All Rights Reserved. */

#ifndef __GIREPO_GDK_CUSTOM_H__
#define __GIREPO_GDK_CUSTOM_H__

#include <glib.h>
#include <gdk/gdk.h>

G_BEGIN_DECLS

GdkRectangle* gdk_rectangle_new    (void);
guint         gdk_event_get_symbol (GdkEvent *event);

G_END_DECLS

#endif  /* __GIREPO_GDK_CUSTOM_H__ */
