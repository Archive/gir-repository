#include <glib-object.h>

extern GType dbus_connection_get_g_type(void);
extern GType dbus_message_get_g_type(void);

GType dbus_connection_get_type(void)
{
  return dbus_connection_get_g_type();
}

GType dbus_message_get_type(void)
{
  return dbus_message_get_g_type();
}
