#include <glib.h>
#include <dbus/dbus-glib.h>

typedef gint32 dbus_int32_t;
typedef gboolean dbus_bool_t;

typedef struct DBusConnection DBusConnection;
typedef struct DBusError DBusError;
typedef struct DBusMessage DBusMessage;
typedef struct DBusMessageIter DBusMessageIter;
typedef struct DBusPendingCall DBusPendingCall;

dbus_bool_t  dbus_threads_init_default (void);

typedef enum
{
  DBUS_BUS_SESSION,    /**< The login session bus */
  DBUS_BUS_SYSTEM,     /**< The systemwide bus */
  DBUS_BUS_STARTER     /**< The bus that started us, if any */
} DbusBusType;

/* Scanner refuses to pick the enum up if it doesn's start
 * with Dbus, we're fixing it with regexps on the gir afterwards
 * for now
 */

GType dbus_connection_get_type (void);
GType dbus_message_get_type (void);
