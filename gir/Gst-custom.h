/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */

/**
 * gst_init:
 * @argc: (inout): argument count
 * @argv: (inout) (array length=argc) (allow-none): arguments
 **/

/**
 * gst_element_query_position:
 * @element:
 * @format: (inout)
 * @cur: (out)
 */

/**
 * gst_element_query_duration:
 * @element:
 * @format: (inout)
 * @duration: (out)
 */

/* https://bugzilla.gnome.org/show_bug.cgi?id=605189 */
/**
 * gst_element_get_state:
 * @element:
 * @state: (out):
 * @pending: (out):
 * @timeout:
 */

/* https://bugzilla.gnome.org/show_bug.cgi?id=622025 */

/**
 * GstPadIntLinkFunction:
 * @pad: The #GstPad to query.
 *
 * The signature of the internal pad link function.
 *
 * Returns: (element-type GstPad): (transfer none): returns
 */
