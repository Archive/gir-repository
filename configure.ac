#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

dnl the gr version number
m4_define(gr_major_version, 0)
m4_define(gr_minor_version, 6)
m4_define(gr_micro_version, 6)
m4_define(gr_version, gr_major_version.gr_minor_version.gr_micro_version)

AC_PREREQ(2.59)
AC_INIT(gir-repository, gr_version,
        http://bugzilla.gnome.org/enter_bug.cgi?product=glib&component=introspection)
AC_SUBST(ACLOCAL_AMFLAGS, "-I m4 -I .")
AM_INIT_AUTOMAKE([-Wno-portability])
AM_MAINTAINER_MODE
AC_PROG_LIBTOOL
AC_CONFIG_HEADER([config.h])
AC_CONFIG_MACRO_DIR([m4])

# GObject Introspection
GOBJECT_INTROSPECTION_REQUIRE(0.9.5)

AC_ARG_WITH([skipped-gir-modules], [AS_HELP_STRING([--with-skipped-gir-modules], [Comma-separated list of namespaces to skip building (e.g. "Pango,Gst")])], [], [])
# This way we can grep for ,foo,
with_skipped_gir_modules=,$with_skipped_gir_modules,

m4_define([GIR_CHECK], [
  m4_define([modname], m4_if([$3], [],
    translit($1, 'a-z', 'A-Z'),
    [$3]))
  if echo $with_skipped_gir_modules | grep -q ,$1,; then
    echo "Checking for $1...skipped"
    have_$1=false
  else
    PKG_CHECK_MODULES(modname, $2, have_$1=true, have_$1=false)
  fi
  AM_CONDITIONAL([BUILD_]modname, $have_$1)
])  

dnl dbus
GIR_CHECK(DBus, dbus-glib-1)

dnl atk - has upstream support since 1.29.4
GIR_CHECK(Atk, atk >= 1.12.0)

dnl pango - has upstream support since 1.25.4
GIR_CHECK(Pango, pango >= 1.16.0)

GIR_CHECK(PangoXft, pangoxft >= 1.16.0)

dnl poppler
GIR_CHECK(Poppler, poppler-glib >= 0.8)

dnl gtk+ - has upstream support since 2.19.2
GIR_CHECK(Gtk, gtk+-2.0 >= 1.12.0)
GDK_TARGET=
GDK_TARGET_GIRS=
if $have_Gtk; then
    PKG_CHECK_MODULES(GDKPIXBUF, gdk-pixbuf-2.0)
    PKG_CHECK_MODULES(GDK, gdk-2.0)

    GDK_TARGET="$($PKG_CONFIG --variable=target gtk+-2.0)"
    if test "$GDK_TARGET" = x11; then
        GDK_TARGET_GIRS=xlib-2.0
    fi
fi
AC_SUBST(GDK_TARGET)
AC_SUBST(GDK_TARGET_GIRS)

dnl gconf
GIR_CHECK(GConf, gconf-2.0)

dnl soup
GIR_CHECK(Soup, libsoup-2.4)

dnl babl
GIR_CHECK(BABL, babl >= 0.1.2)

dnl nautilus-extension
GIR_CHECK(Nautilus, libnautilus-extension >= 2.22)

dnl gnome-keyring
GIR_CHECK(GnomeKeyring, gnome-keyring-1)

dnl webkit - has upstream support since 1.1.16
GIR_CHECK(WebKit, webkit-1.0 >= 1.0)

dnl notify
GIR_CHECK(Notify, libnotify)

dnl gnio
GIR_CHECK(Gnio, gnio)

dnl gstreamer supports introspection starting from 0.10.25
GIR_CHECK(Gst, gstreamer-0.10 >= 0.10.0, GSTREAMER)

dnl gstbufferlist.h, gsttaskpool.h were added in 0.10.24 (and in
dnl prereleases of that)
have_gstbufferlist_h=false
if $have_gstreamer ; then
   save_CPPFLAGS="$CPPFLAGS"; CPPFLAGS="$CPPFLAGS $GSTREAMER_CFLAGS"
   AC_CHECK_HEADER(gst/gstbufferlist.h, [have_gstbufferlist_h=true])
   CPPFLAGS="$save_CPPFLAGS"
fi
AM_CONDITIONAL(HAVE_GSTBUFFERLIST_H, $have_gstbufferlist_h)

have_gsttaskpool_h=false
if $have_gstreamer ; then
   save_CPPFLAGS="$CPPFLAGS"; CPPFLAGS="$CPPFLAGS $GSTREAMER_CFLAGS"
   AC_CHECK_HEADER(gst/gsttaskpool.h, [have_gsttaskpool_h=true])
   CPPFLAGS="$save_CPPFLAGS"
fi
AM_CONDITIONAL(HAVE_GSTTASKPOOL_H, $have_gsttaskpool_h)

dnl gst-plugins-base supports introspection starting from 0.10.25
PKG_CHECK_MODULES(GSTREAMER_PLUGINS_BASE, [gstreamer-plugins-base-0.10 >= 0.10.0],
                  have_gstreamer_plugins_base=true,
                  have_gstreamer_plugins_base=false)
dnl make sure that disabling Gst also disables gst-plugins-base
AM_CONDITIONAL(BUILD_GSTREAMER_PLUGINS_BASE, $have_Gst && $have_gstreamer_plugins_base)

dnl gtksourceview
GIR_CHECK(GtkSourceView, gtksourceview-2.0)

dnl vte
GIR_CHECK(Vte, vte)

dnl goocanvas
GIR_CHECK(GooCanvas, goocanvas)

dnl mission-control (disabled for now, it has odd structs and isn't useful yet)
dnl PKG_CHECK_MODULES(MISSIONCONTROL, libmissioncontrol,
dnl                  have_missioncontrol=true, have_missioncontrol=false)
have_missioncontrol=false
AM_CONDITIONAL(BUILD_MISSIONCONTROL, $have_missioncontrol)

dnl gssdp
GIR_CHECK(GSSDP, gssdp-1.0)

dnl gupnp, not using macro due to conditional on have_gssdp too
if test $have_gssdp && echo $with_skipped_gir_modules | grep -q ,GUPNP,; then
    PKG_CHECK_MODULES(GUPNP, gupnp-1.0,
                      have_gupnp=true, have_gupnp=false)
else
  have_gupnp=false
fi
AM_CONDITIONAL(BUILD_GUPNP, $have_gupnp)

dnl avahi-gobject
GIR_CHECK(Avahi, avahi-core >= 0.6 avahi-gobject >= 0.6)

dnl unique
GIR_CHECK(Unique, unique-1.0 >= 1.0.0)

dnl gmenu
GIR_CHECK(GMenu, libgnome-menu)

dnl wnck
GIR_CHECK(Wnck, libwnck-1.0)

AC_CONFIG_FILES([Makefile
                 gir/Makefile])

AC_OUTPUT
