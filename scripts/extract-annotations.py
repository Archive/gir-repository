import operator
import sys

from codegen.defsparser import DefsParser

p = DefsParser(sys.argv[1])
p.startParsing(sys.argv[1])

for method in sorted(p.functions, key=operator.attrgetter('c_name')):
    nulls = []
    for param in method.params:
        null = getattr(param, 'pnull', 0)
        if not null:
            continue
        nulls.append(param)
    if not nulls:
        continue
    print '/**'
    print ' * %s:' % (method.c_name,)
    for param in nulls:
        print ' * @%s: (allow-none):' % (param.pname,)
    print ' */'
    print
